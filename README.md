# Grafana on Fargate

## How to
Setup Module >> Get Grafana password from Parameter Store >> Log into database and run the sql queries below 
adding the password for Grafana.

## Setup your DB

For Grafana to work properly using ECS with Fargate you need to setup your 
database with the following script:

```
CREATE DATABASE grafana;
CREATE USER grafana WITH ENCRYPTED PASSWORD 'your_grafana_password';
GRANT ALL PRIVILEGES ON DATABASE grafana TO grafana;
```

## Secrets

The Task Definition will get the environment vars from the Grafana Container
on the Systems Manager Parameter Store. Check the `ssm.tf` for more information.

## Module Setup

```
module "grafana" {
  source                = "git@gitlab.com:terraform147/grafana.git"
  app_name              = var.app_name
  cloudwatch_name       = var.cloudwatch_name
  container_port        = 3000
  domain                = var.domain
}
```

---
Check `variables.tf` for the vars that uses `default` values and initialize them on the
module setup if you want to use different values.

## Authentication

You can follow the [Grafana Cloudwatch](https://grafana.com/docs/grafana/latest/features/datasources/cloudwatch/) docs
to choose your authentication to AWS.