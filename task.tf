data "template_file" "this" {
  template = file("${path.module}/task-definitions/task.json")

  vars = {
    image               = var.image_url
    container_name      = var.container_name
    container_port      = var.container_port
    log_group           = var.cloudwatch_name
    desired_task_cpu    = var.desired_task_cpu
    desired_task_memory = var.desired_task_memory
    secrets             = jsonencode(local.secrets)
  }
}

resource "aws_ecs_task_definition" "this" {
  family                   = "${var.app_name}-${var.container_name}"
  container_definitions    = data.template_file.this.rendered
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = var.desired_task_cpu
  memory                   = var.desired_task_memory
  execution_role_arn       = aws_iam_role.task.arn
  task_role_arn            = aws_iam_role.task.arn

  tags = {
    Product = var.app_name
  }
}

resource "aws_ecs_service" "this" {
  name                    = var.container_name
  task_definition         = aws_ecs_task_definition.this.arn
  cluster                 = data.aws_ecs_cluster.selected.id
  launch_type             = "FARGATE"
  desired_count           = var.desired_tasks
  enable_ecs_managed_tags = true

  network_configuration {
    security_groups  = [aws_security_group.this.id]
    subnets          = tolist(data.aws_subnet_ids.private.ids)
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.grafana.arn
    container_name   = var.container_name
    container_port   = var.container_port
  }

  lifecycle {
    ignore_changes = [desired_count]
  }

  tags = {
    Product = var.app_name
  }

  depends_on = [aws_lb_target_group.grafana]
}
