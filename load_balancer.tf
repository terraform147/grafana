resource "aws_lb_target_group" "grafana" {
  name        = "${var.app_name}-${var.container_name}-lb-tg"
  port        = var.container_port
  protocol    = "HTTP"
  vpc_id      = data.aws_vpc.selected.id
  target_type = "ip"

  lifecycle {
    create_before_destroy = true
  }

  health_check {
    matcher  = "302"
    path     = "/"
    port     = var.container_port
    timeout  = 30
    interval = 40
  }

  tags = {
    Product = var.app_name
  }

  depends_on = [data.aws_lb.selected]
}

resource "aws_lb_listener_rule" "grafana" {
  listener_arn = data.aws_lb_listener.selected443.arn

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.grafana.arn
  }

  condition {
    host_header {
      values = [local.url]
    }
  }
}
