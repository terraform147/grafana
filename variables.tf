variable "app_name" {
  description = "Application name with environment"
  type        = string
}

variable "cloudwatch_name" {
  description = "Cloudwatch log group name"
  type        = string
}

variable "container_port" {
  description = "ALB target port"
  default     = 3000
}

variable "container_name" {
  description = "Name of the docker image"
  type        = string
  default     = "grafana"
}

variable "desired_tasks" {
  description = "Number of tasks to run"
  default     = 1
}

variable "desired_task_cpu" {
  description = "Task CPU Limit"
  default     = 1024
}

variable "desired_task_memory" {
  description = "Task Memory Limit"
  default     = 2048
}

variable "domain" {
  description = "Product domain"
  type        = string
}

variable "health_check_url" {
  description = "Health check"
  type        = string
  default     = "/"
}

variable "image_url" {
  description = "Name of the docker image"
  type        = string
  default     = "grafana/grafana"
}

variable "grafana_policy_arn" {
  description = "Arn of Grafana Policy"
  type        = string
}

variable "region" {
  description = "Aws region for policies"
  type        = string
  default     = "us-east-1"
}

data "aws_vpc" "selected" {
  filter {
    name   = "tag:Name"
    values = ["VPC_${upper(replace(var.app_name, "-", "_"))}"]
  }
}

data "aws_subnet_ids" "private" {
  vpc_id = data.aws_vpc.selected.id

  tags = {
    Tier = "Private"
  }
}

data "aws_subnet" "private" {
  for_each = data.aws_subnet_ids.private.ids
  id       = each.value
}

data "aws_ecs_cluster" "selected" {
  cluster_name = "${var.app_name}-ecs-cluster"
}

data "aws_lb" "selected" {
  name = "${var.app_name}-external-lb"
}

data "aws_security_group" "selected" {
  name = "${var.app_name}-external-lb-sg"
}

data "aws_lb_listener" "selected443" {
  load_balancer_arn = data.aws_lb.selected.arn
  port              = 443
}

data "aws_db_instance" "database" {
  db_instance_identifier = "${var.app_name}-db"
}

data "aws_kms_key" "selected" {
  key_id = "alias/${var.app_name}"
}

data "aws_route53_zone" "selected" {
  name = var.domain
}

locals {
  url = "grafana.${var.domain}"
}

data "aws_caller_identity" "current" {}

data "aws_iam_policy" "grafana" {
  arn = var.grafana_policy_arn
}
