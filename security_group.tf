resource "aws_security_group" "this" {
  name   = "${var.app_name}-${var.container_name}-sg"
  vpc_id = data.aws_vpc.selected.id

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "internet"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
  }

  tags = {
    Name    = "${var.app_name}-${var.container_name}-sg"
    Product = var.app_name
  }
}

# Allow access from load balancer
resource "aws_security_group_rule" "load_balancer" {
  description              = "load-balancer"
  from_port                = var.container_port
  to_port                  = var.container_port
  protocol                 = "tcp"
  type                     = "ingress"
  security_group_id        = aws_security_group.this.id 
  source_security_group_id = data.aws_security_group.selected.id
}
