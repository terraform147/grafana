# Cluster Execution Role
resource "aws_iam_role" "task" {
  name               = "${var.app_name}-grafana-ecs-task-role"
  assume_role_policy = file("${path.module}/policies/assume_role_task.json")
}

# AmazonECSTaskExecutionRolePolicy
resource "aws_iam_role_policy_attachment" "this" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
  role       = aws_iam_role.task.id
}

data "template_file" "ssm_policy" {
  template = file("${path.module}/policies/ecs-ssm-role-policy.json")

  vars = {
    ssm_path    = "arn:aws:ssm:${var.region}:${data.aws_caller_identity.current.account_id}:parameter/${var.app_name}/${var.container_name}/*"
    kms_key_arn = "arn:aws:kms:${var.region}:${data.aws_caller_identity.current.account_id}:key/${data.aws_kms_key.selected.id}"
  }
}

resource "aws_iam_role_policy" "ecs_ssm_role_policy" {
  name   = "${var.app_name}-${var.container_name}-ssm-role-policy"
  policy = data.template_file.ssm_policy.rendered
  role   = aws_iam_role.task.id
}

resource "aws_iam_role_policy_attachment" "grafana" {
  role       = aws_iam_role.task.name
  policy_arn = data.aws_iam_policy.grafana.arn
}