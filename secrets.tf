locals {
  secrets = [{
    name      = "GF_DATABASE_TYPE"
    valueFrom = aws_ssm_parameter.db_type.arn
    },
    {
      name      = "GF_DATABASE_HOST"
      valueFrom = aws_ssm_parameter.db_host.arn
    },
    {
      name      = "GF_DATABASE_USER"
      valueFrom = aws_ssm_parameter.db_user.arn
    },
    {
      name      = "GF_DATABASE_NAME"
      valueFrom = aws_ssm_parameter.db_name.arn
    },
    {
      name      = "GF_DATABASE_PASSWORD"
      valueFrom = aws_ssm_parameter.db_pass.arn
  }]
}
